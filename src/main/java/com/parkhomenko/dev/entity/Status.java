package com.parkhomenko.dev.entity;

/**
 * Created by dmitro on 5/5/15.
 */
public enum Status {
    IN_PROCESS, DELIVERED, REJECTED
}
