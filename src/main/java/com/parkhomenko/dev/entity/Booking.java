package com.parkhomenko.dev.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Asus1 on 27.04.2015.
 */

@Entity
public class Booking {
    @Id
    @GeneratedValue
    private Integer id;
    private Date date;
    private String description;

    @ManyToOne
    @JoinColumn(name="PERSON_ID")
    private Person person;

    @ManyToOne
    @JoinColumn(name="HOTEL_ID")
    private Hotel hotel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}