package com.parkhomenko.dev.repo;

import com.parkhomenko.dev.entity.Person;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Asus1 on 29.04.2015.
 */
public interface PersonRepo extends CrudRepository<Person, Integer> {
}
