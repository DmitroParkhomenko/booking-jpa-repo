package com.parkhomenko.dev.repo;

import com.parkhomenko.dev.entity.Hotel;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Asus1 on 28.04.2015.
 */
public interface HotelRepo extends PagingAndSortingRepository<Hotel, Integer> {
}
