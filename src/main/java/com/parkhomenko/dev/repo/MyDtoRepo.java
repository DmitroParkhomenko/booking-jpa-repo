package com.parkhomenko.dev.repo;

import com.parkhomenko.dev.entity.MyDto;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Asus1 on 29.04.2015.
 */
public interface MyDtoRepo extends CrudRepository<MyDto, Integer> {
}
