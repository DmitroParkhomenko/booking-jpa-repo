package com.parkhomenko.dev.repo;

import com.parkhomenko.dev.entity.Booking;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Asus1 on 27.04.2015.
 */
public interface BookingRepo extends CrudRepository<Booking, Integer> {
    Long countByDate(Date date);
    List<Booking> removeByDate(Date date);
}
