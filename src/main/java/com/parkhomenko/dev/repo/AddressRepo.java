package com.parkhomenko.dev.repo;

import com.parkhomenko.dev.entity.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Asus1 on 29.04.2015.
 */
public interface AddressRepo extends CrudRepository<Address, Integer> {
    // Enabling ignoring case for all suitable properties
    List<Address> findByCityAndCountryAllIgnoreCase(String city, String country);
}
