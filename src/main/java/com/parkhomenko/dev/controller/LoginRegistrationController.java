package com.parkhomenko.dev.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by dmitro on 5/4/15.
 */

@Controller
public class LoginRegistrationController {

    @RequestMapping("/login-register")
    public String index() {
        return "login-and-register-tabbed-form";
    }
}
