package com.parkhomenko.dev.controller;

import com.parkhomenko.dev.entity.MyDto;
import com.parkhomenko.dev.service.MyDtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Asus1 on 29.04.2015.
 */

@Controller
public class MyFormController {

    @Autowired
    private MyDtoService myDtoService;

    @RequestMapping("/form")
    public String login() {
        return "json-form";
    }

    @RequestMapping(value = "/saveProduct", method = {RequestMethod.POST})
    @ResponseBody
    public void login(HttpServletResponse response, @RequestBody MyDto dto) throws IOException {
        myDtoService.save(dto);
        response.getWriter().println("saved");
    }
}
