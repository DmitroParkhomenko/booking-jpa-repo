package com.parkhomenko.dev.controller;

import com.parkhomenko.dev.entity.User;
import com.parkhomenko.dev.service.OrderService;
import com.parkhomenko.dev.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.List;

/**
 * Created by dmitro on 5/4/15.
 */

@Controller
@RequestMapping("/order")
public class BinController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;

    @RequestMapping("/bin")
    public String index() {
        return "rubbish-bin";
    }

    //This API will be under Spring Security -> it is secure uri!!!!
    @RequestMapping(value = "/do", method = RequestMethod.POST)
    public ResponseEntity<String> account(@RequestBody List<Integer> itemsIds, Principal principal) {
        String userName = principal.getName();
        User user = userService.getUserByName(userName);
        orderService.makeOrder(user, itemsIds);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
