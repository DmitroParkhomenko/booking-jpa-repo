package com.parkhomenko.dev.controller;

import com.parkhomenko.dev.utils.ImageResizerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Asus1 on 30.04.2015.
 */

@Controller
public class ImageUploaderController {

    private static final Logger logger = LoggerFactory.getLogger(ImageUploaderController.class);

    @RequestMapping(value = "/uploadFile", method = RequestMethod.GET)
    public String getUploadView() {
        return "upload";
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    public String uploadFileHandler(@RequestParam("name") String name, @RequestParam("file") MultipartFile file) throws IOException {

        if (!file.isEmpty()) {
            String rootPath = "c:" + File.separator + "images";

            File dir = new File(rootPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            BufferedImage bufferedImage = ImageResizerUtil.resizeImage(file);

            File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
            ImageIO.write(bufferedImage, "jpg", serverFile);

            logger.info("Server File Location = " + serverFile.getAbsolutePath());

            return "You successfully uploaded file = " + name;
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }

    @RequestMapping(value = "/showFile", method = RequestMethod.GET)
    public String getShowAllImagesView() {
        return "show-images";
    }

    @RequestMapping(value = "/getImage/{imageId}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] loadImage(@PathVariable String imageId) throws IOException {
        String url = "http://www.appname.com/" + imageId;
        URL imageURL = new URL(url);
        BufferedImage originalImage = ImageIO.read(imageURL);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", baos);
        return baos.toByteArray();
    }
}
