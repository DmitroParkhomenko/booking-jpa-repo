package com.parkhomenko.dev.service;

import com.parkhomenko.dev.entity.Booking;
import com.parkhomenko.dev.repo.BookingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Asus1 on 27.04.2015.
 */

@Service
@Transactional
public class BookingService {

    @Autowired
    private BookingRepo bookingRepo;

    public List<Booking> findAll() {
        List<Booking> all = new ArrayList<>();

        Iterable<Booking> iter = bookingRepo.findAll();

        for (Booking booking : iter) {
            all.add(booking);
        }

        return all;
    }

    public Booking save(Booking entity) {
        return bookingRepo.save(entity);
    }

    public void delete(Integer id) {
        bookingRepo.delete(id);
    }

    public Long countByDate(Date date) {
        return bookingRepo.countByDate(date);
    }

    public List<Booking> removeByDate(Date date) {
        return bookingRepo.removeByDate(date);
    }
}
