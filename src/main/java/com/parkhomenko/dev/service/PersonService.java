package com.parkhomenko.dev.service;

import com.parkhomenko.dev.entity.Hotel;
import com.parkhomenko.dev.entity.Person;
import com.parkhomenko.dev.repo.PersonRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by Asus1 on 29.04.2015.
 */

@Service
@Transactional
public class PersonService {

    @Autowired
    private PersonRepo personRepo;

    public Person save(Person entity) {
        return personRepo.save(entity);
    }
}
