package com.parkhomenko.dev.service;

import com.parkhomenko.dev.entity.Hotel;
import com.parkhomenko.dev.repo.HotelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Asus1 on 28.04.2015.
 */

@Service
@Transactional
public class HotelService {

    @Autowired
    private HotelRepo hotelRepo;

    public Hotel save(Hotel entity) {
        return hotelRepo.save(entity);
    }

    public List<Hotel> test(int page, int size) {
        Page<Hotel> pages = hotelRepo.findAll(new PageRequest(page, size));
        return pages.getContent();
    }
}
