package com.parkhomenko.dev.service;

import com.parkhomenko.dev.entity.MyDto;
import com.parkhomenko.dev.repo.MyDtoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by Asus1 on 29.04.2015.
 */

@Service
@Transactional
public class MyDtoService {

    @Autowired
    MyDtoRepo myDtoRepo;

    public MyDto save(MyDto entity) {
        return myDtoRepo.save(entity);
    }
}
