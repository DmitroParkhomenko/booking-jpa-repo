package com.parkhomenko.dev.service;

import com.parkhomenko.dev.entity.Address;
import com.parkhomenko.dev.repo.AddressRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Asus1 on 29.04.2015.
 */

@Service
@Transactional
public class AddressService {

    @Autowired
    private AddressRepo addressRepo;

    public Address save(Address entity) {
        return addressRepo.save(entity);
    }

    public List<Address> findAllAddressByCityAndCountry(String city, String country) {
        return addressRepo.findByCityAndCountryAllIgnoreCase(city, country);
    }
}
