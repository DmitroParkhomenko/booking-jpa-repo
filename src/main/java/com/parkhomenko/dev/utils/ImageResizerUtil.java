package com.parkhomenko.dev.utils;

import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Asus1 on 30.04.2015.
 */
public class ImageResizerUtil {

    private static final int IMG_WIDTH = 100;
    private static final int IMG_HEIGHT = 100;

    public static BufferedImage resizeImage(MultipartFile file){
        BufferedImage resizeImageJpg = null;

        try{
            byte[] bytes = file.getBytes();
            InputStream in = new ByteArrayInputStream(bytes);
            BufferedImage originalImage = ImageIO.read(in);
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
            resizeImageJpg = resizeImage(originalImage, type);
        }catch(IOException e){
            System.out.println(e.getMessage());
        }

        return resizeImageJpg;

    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type){
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        return resizedImage;
    }
}
