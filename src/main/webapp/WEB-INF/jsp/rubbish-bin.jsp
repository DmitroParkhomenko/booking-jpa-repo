<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<script type="text/javascript">
    $(document).ready(function () {

        $('.col-md-offset-3').css('margin-left', '0');

        (function () {
            $("#doOrderBtn").click(function () {

                var data = [24, 25, 26];

                $.ajax({
                    type: "POST",
                    accept: 'application/json',
                    url: "do",
                    contentType: 'application/json',
                    data: JSON.stringify(data),
                    success: function () {
                        console.log("success!!!");
                    },
                    error: function () {
                        console.log("error!!!");
                    }
                });
            });
        })();
    })
</script>

<input id="doOrderBtn" type="button" value="Do order">

<!-- Button trigger modal -->
<button type="button" data-toggle="modal" data-target="#myModal">
    Sing in and do order
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <jsp:include page="../jsp/login-and-register-tabbed-form.jsp" />
            </div>
        </div>
    </div>
</div>