<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            (function () {
                $("#myform").submit(function (event) {

                    event.preventDefault();

                    var name = $('#name').val();
                    var vendor = $('#vendor').val();
                    var price = $('#price').val();

                    var data = {
                        name: name,
                        vendor: vendor,
                        price: price
                    };

                    $.ajax({
                        type: "POST",
//                        accept : "text/html",
//                        dataType: 'json',
                        url: "saveProduct",
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function () {
                            console.log("success!!!");
                        },
                        error: function () {
                            console.log("error!!!");
                        }
                    });
                });
            })();
        })
    </script>
</head>

<body>
<form id="myform">
    <label for="name">Name</label>
    <input type="text" id="name" value="My test name">

    <label for="vendor">Vendor</label>
    <input type="text" id="vendor" value="My test vendor">

    <label for="price">Price</label>
    <input type="text" id="price" value="24">
    <br><br>
    <input type="submit" value="Submit">
</form>
</body>
</html>