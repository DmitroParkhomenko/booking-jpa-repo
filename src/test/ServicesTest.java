import com.parkhomenko.dev.entity.Address;
import com.parkhomenko.dev.entity.Booking;
import com.parkhomenko.dev.entity.Hotel;
import com.parkhomenko.dev.entity.Person;
import com.parkhomenko.dev.service.AddressService;
import com.parkhomenko.dev.service.BookingService;
import com.parkhomenko.dev.service.HotelService;
import com.parkhomenko.dev.service.PersonService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * Created by Asus1 on 27.04.2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class ServicesTest {
    @Autowired
    private BookingService bookingService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private PersonService personService;

    @BeforeClass
    public void initDb() {
        Address address = new Address();
        address.setCity("Riga");
        address.setCountry("Latvia");
        address.setStreet("Lagunas 12");

//        addressService.save(address);



//        Address address2 = new Address();
//        address2.setCity("Riga");
//        address2.setCountry("Latvia");
//        address2.setStreet("Ielas 21");
//
//        addressService.save(address2);
//
//        Address address3 = new Address();
//        address3.setCity("Kiev");
//        address3.setCountry("Ukraine");
//        address3.setStreet("Baugas 14");
//
//        addressService.save(address3);

        //////

        Hotel hotel = new Hotel();
        hotel.setName("Riga Plaza");
        hotel.setAddress(address);

        hotelService.save(hotel);
        ////

//        Person person = new Person();
//        person.setAge(24);
//        person.setFirstname("dima");
//        person.setLastname("parkhomenko");
//
//        personService.save(person);
//        ////
//
//        Booking booking = new Booking();
//        booking.setDate(new Date());
//        booking.setDescription("first booking desc");
//        booking.setPerson(person);
//        booking.setHotel(hotel);
//
//        bookingService.save(booking);
        ////
    }

    @Test
    public void test1() {
        Assert.assertTrue(bookingService.findAll().size() > 0);
    }

    @Test
    public void test2() {
        List<Address> addresses = addressService.findAllAddressByCityAndCountry("Riga", "Latvia");

        Assert.assertTrue(addresses.size() == 2);
    }
}
