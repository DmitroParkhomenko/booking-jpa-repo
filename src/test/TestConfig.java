import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by Asus1 on 27.04.2015.
 */

@Configuration
@ImportResource("/persistenceApplicationContext.xml")
public class TestConfig {
}
